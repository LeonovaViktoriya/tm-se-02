package ru.leonova.tm;

import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.repository.ProjectRepository;
import ru.leonova.tm.repository.TaskRepository;

import java.util.Scanner;
import java.util.UUID;

public class Application {

    public static void main(String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        Scanner scanner = new Scanner(System.in);
        Scanner input = new Scanner(System.in);
        ProjectRepository projectRepository = new ProjectRepository();
        TaskRepository taskManager = new TaskRepository();
        String text;
        do {
            text = scanner.nextLine();
            switch (text.toLowerCase()) {
                case "create-p":
                    System.out.print("[CREATE PROJECT]\nINTER NAME: \n");
                    String name = scanner.nextLine();
                    projectRepository.createProject(name);
                    System.out.println("Project created");
                    break;
                case "create-t":
                    if (projectRepository.isEmptyProjectList()) {
                        System.out.println("You have no project for task");
                        break;
                    }
                    System.out.println("[CREATE TASK]\nList projects:");
                    projectRepository.showProjectList();
                    System.out.println("\nSELECT ID PROJECT: ");
                    String id = scanner.nextLine();
                    UUID idProject = UUID.fromString(id);
                    System.out.println("Enter name task: ");
                    String nn = input.nextLine();
                    taskManager.createTask(idProject, nn);
                    System.out.println("Task created");
                    break;
                case "list-p":
                    if (projectRepository.isEmptyProjectList()) {
                        System.out.println("You have no projects");
                        return;
                    }
                    System.out.println("\n[PROJECT LIST]");
                    projectRepository.showProjectList();
                    break;
                case "list-t":
                    if (taskManager.isEmptyTaskList()) {
                        System.out.println("You have no tasks");
                        break;
                    }
                    System.out.println("\n[TASK LIST]");
                    taskManager.showTaskList();
                    break;
                case "del-p-id":
                    if (projectRepository.isEmptyProjectList()) {
                        System.out.println("You have no projects");
                        break;
                    }
                    System.out.println("[DELETE PROJECT BY ID]\nList projects:");
                    projectRepository.showProjectList();
                    System.out.println("\nEnter id project:");
                    id = scanner.nextLine();
                    idProject = UUID.fromString(id);
                    Project p = projectRepository.findProjectByID(idProject);
                    if (p == null) {
                        System.out.println("This project not found");
                        break;
                    }
                    taskManager.deleteTasksByIdProject(p.getPrID());
                    projectRepository.deleteProject(p);
                    System.out.println("Project with id: " + p.getPrID() + " with his tasks are removed");
                    break;
                case "del-t-id":
                    if (taskManager.isEmptyTaskList()) {
                        System.out.println("You have no tasks");
                        break;
                    }
                    System.out.println("[DELETE TASK BY ID]\n[Task list:]");
                    taskManager.showTaskList();
                    System.out.println("Enter id:");
                    String idTask = scanner.nextLine();
                    UUID uuidTask = UUID.fromString(idTask);
                    Task t = taskManager.findTaskByID(uuidTask);
                    if (t == null) {
                        System.out.println("This task not found");
                        break;
                    }
                    taskManager.deleteTaskByName(t);
                    System.out.println("Task " + t.getName() + " remove");
                    break;
                case "up-p":
                    if (projectRepository.isEmptyProjectList()) {
                        System.out.println("\nYou have no projects");
                        break;
                    }
                    System.out.println("[UPDATE NAME PROJECT]\nList projects");
                    projectRepository.showProjectList();
                    System.out.println("\nEnter id project:");
                    idProject = UUID.fromString(scanner.nextLine());
                    p = projectRepository.findProjectByID(idProject);
                    if (p == null) {
                        System.out.println("Project with this name not found");
                        break;
                    }
                    System.out.println("Enter new name for this project:");
                    name = scanner.nextLine();
                    projectRepository.updateProject(p, name);
                    System.out.println("Task " + p.getName() + " modified like " + name);
                    break;
                case "up-t":
                    if (taskManager.isEmptyTaskList()) {
                        System.out.println("You have no tasks");
                        break;
                    }
                    System.out.println("[UPDATE NAME TASK]\nList tasks:");
                    taskManager.showTaskList();
                    System.out.println("\nEnter id task for rename:");
                    name = scanner.nextLine();
                    t = taskManager.findTaskByName(name);
                    if (t == null) {
                        System.out.println("This task not found");
                        break;
                    }
                    System.out.println("enter new name for task:");
                    name = scanner.nextLine();
                    taskManager.updateTaskName(t, name);
                    System.out.println("Task " + name + " modified like " + name);
                    break;
                case "del-all-p":
                    projectRepository.deleteAllProjects();
                    System.out.println("All projects remove");
                    break;
                case "del-all-t":
                    taskManager.deleteAllTasks();
                    System.out.println("All tasks remove");
                    break;
                case "del-all-t-t-p":
                    System.out.println("[DELETE ALL TASKS OF THE SELECTED PROJECT]\n");
                    projectRepository.showProjectList();
                    System.out.println("\nSELECT ID PROJECT: ");
                    idProject = UUID.fromString(scanner.nextLine());
                    taskManager.deleteTasksByIdProject(idProject);
                    System.out.println("All tasks of this project are deleted");
                    break;
                case "help":
                    System.out.println("\nhelp - show available commands");
                    System.out.println("\ncreate-p - create new project");
                    System.out.println("\ncreate-t - create new task for current project");
                    System.out.println("\nlist-p - show project list");
                    System.out.println("\nlist-t - show project list");
                    System.out.println("\ndel-p-name - delete project by name");
                    System.out.println("\ndel-p-id - delete project by id");
                    System.out.println("\nup-p - update project");
                    System.out.println("\nup-t - update project's task's name");
                    System.out.println("\ndel-t-name - delete task by name");
                    System.out.println("\ndel-all-t - delete all task");
                    System.out.println("\ndel-all-t-t-p - delete all task of the selected project");
                    System.out.println("\ndel-all-p - delete all projects");
            }
        }
        while (!text.equals("exit"));
    }

}
