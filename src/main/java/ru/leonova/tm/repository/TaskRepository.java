package ru.leonova.tm.repository;

import ru.leonova.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TaskRepository {
    private List<Task> taskList = new ArrayList<>();

    public boolean isEmptyTaskList() {
        return taskList.isEmpty();
    }

    public void createTask(UUID projectId, String taskName) {
        Task task = new Task(taskName, projectId);
        taskList.add(task);
    }

    public void updateTaskName(Task t, String taskName) {
        t.setName(taskName);
    }

    public void showTaskList() {
        int i = 0;
        for (Task task : taskList) {
            i++;
            System.out.println(i + ". ID PROJECT: "+ task.getPrID()+ ", NAME: " + task.getName());
        }
    }

    public Task findTaskByName(String name) {
        for (Task t : taskList) {
            if (t.getName().equalsIgnoreCase(name)) {
                return t;
            }
        }
        return null;
    }

    public Task findTaskByID(UUID uuid) {
        for (Task t : taskList) {
            if (t.getTaskID().equals(uuid)) {
                return t;
            }
        }
        return null;
    }

    public void deleteTaskByName(Task t) {
        taskList.remove(t);
    }

    public void deleteTasksByIdProject(UUID prID) {
        taskList.removeIf(task -> prID.equals(task.getPrID()));
    }

    public void deleteAllTasks() {
        taskList.clear();
    }

}

