package ru.leonova.tm.repository;

import ru.leonova.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProjectRepository {

    private List<Project> projectList = new ArrayList<>();

    public boolean isEmptyProjectList() {
        return projectList.isEmpty();
    }

    public void createProject(String projectName) {
        Project project = new Project(projectName);
        projectList.add(project);
    }

    public void showProjectList() {
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println(i + ".  ID: " + projectList.get(i).getPrID() + ", NAME: " + projectList.get(i).getName());
        }
    }

    public Project findProjectByID(UUID id) {
        for (Project project : projectList) {
            if (project.getPrID().equals(id)) {
                return project;
            }
        }
        return null;
    }

    public void deleteProject(Project p) {
        projectList.remove(p);
    }

    public void deleteAllProjects() {
        projectList.clear();
    }

    public void updateProject(Project p, String newProjectName) {
         p.setName(newProjectName);
    }

}
