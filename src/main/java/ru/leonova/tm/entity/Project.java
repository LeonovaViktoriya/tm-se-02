package ru.leonova.tm.entity;

import java.util.UUID;

public class Project {

    private String name;
    private UUID prId;

    {
        UUID.randomUUID();
    }

    public Project(String name) {
        this.name = name;
        prId = UUID.randomUUID();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getPrID() {
        return prId;
    }

    public void setPrID(UUID prID) {
        this.prId = prID;
    }

//    public void addTask(String taskName) {
//        Task newTask = new Task(taskName);
//        taskList.add(newTask);
//    }

}
