package ru.leonova.tm.entity;

import java.util.UUID;

public class Task {

    private String name;
    private UUID prID;
    private UUID taskID;
    {
        UUID.randomUUID();
    }

    public Task(String name, UUID prId) {
        this.prID = prId;
        this.name = name;
        taskID = UUID.randomUUID();
    }

    public String getName() {
        return name;
    }

    public UUID getPrID() {
        return prID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getTaskID() {
        return taskID;
    }
}

